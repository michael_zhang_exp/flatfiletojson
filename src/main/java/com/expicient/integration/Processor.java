package com.expicient.integration;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.LinkedHashMap;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

import com.expicient.integration.schema.FlatFileSchema;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class Processor {
	public static void main(String[] args) {
		try {
			// Process command line inputs
			String schemaFileLocation = args[0];
			String datafileLocation = args[1];
			String outputDatafileLocation = args[2];

			// Validate the input and throw errors
			File schemaFile = new File(schemaFileLocation);
			CSVParser parser;
			parser = CSVParser.parse(schemaFile, Charset.defaultCharset(),
					CSVFormat.RFC4180);
			// Parse the schema and load it into an array list
			ArrayList<FlatFileSchema> flatFileSchema = new ArrayList<FlatFileSchema>();
			for (CSVRecord csvRecord : parser) {
				flatFileSchema.add(new FlatFileSchema(csvRecord.get(0), Integer
						.parseInt(csvRecord.get(1)), Integer.parseInt(csvRecord
						.get(2))));
			}

			// Start reading the data file and use schema structure to convert
			// it
			ArrayList<LinkedHashMap<String, String>> outputDataStructure = new ArrayList<LinkedHashMap<String, String>>();
			Scanner scanner = new Scanner(new File(datafileLocation));
			while (scanner.hasNextLine()) {
				String line = scanner.nextLine();
				if (line.length() > 0) {
					parseRecordAndFillData(flatFileSchema, outputDataStructure,
							line);
				}
			}
			scanner.close();

			// Generate output in json
			Gson gson = new GsonBuilder().disableHtmlEscaping().create();
	        FileWriter writer = new FileWriter(outputDatafileLocation);
	        writer.write(gson.toJson(outputDataStructure));
	        writer.close();			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private static void parseRecordAndFillData(ArrayList<FlatFileSchema> flatFileSchema,
			ArrayList<LinkedHashMap<String, String>> outputDataStructure,
			String line) {
		LinkedHashMap<String, String> data = new LinkedHashMap<String, String>();
		for (FlatFileSchema schema : flatFileSchema) {
			data.put(schema.getFieldName(), 
					line.substring(schema.getStartPosition(), 
							schema.getStartPosition() + schema.getLength()));
		}	
		outputDataStructure.add(data);
	}
}
